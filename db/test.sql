/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2019-11-04 15:53:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `student`
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `age` int(3) DEFAULT NULL,
  `class` varchar(20) DEFAULT NULL,
  `grade` varchar(20) DEFAULT NULL,
  `major` varchar(30) DEFAULT NULL,
  `gender` int(1) DEFAULT NULL,
  `date_enrollment` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('1', 'Lin', '18', 'class1', 'grade1', 'major1', '0', '2019-09-01 00:00:00');
INSERT INTO `student` VALUES ('2', 'Lee', '19', 'calss2', 'grade2', 'major2', '1', '2018-09-01 00:00:00');
INSERT INTO `student` VALUES ('4', 'Su', '17', 'Class3', null, null, null, '2019-11-04 10:26:22');
INSERT INTO `student` VALUES ('5', 'Peter', '17', 'Class6', null, null, null, '2019-11-04 10:56:04');
