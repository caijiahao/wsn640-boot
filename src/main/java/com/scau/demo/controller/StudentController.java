package com.scau.demo.controller;

import com.scau.demo.entity.Student;
import com.scau.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController // @Controller + @ResponseBody
@RequestMapping("/superadmin") // 根路由
public class StudentController {
    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/listStudent", method = RequestMethod.GET)
    private Map<String, Object> listStudent() {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        List<Student> list = studentService.getStudentList();
        modelMap.put("studentList", list);
        return modelMap;
    }

    @RequestMapping(value = "/getstudentbyid", method = RequestMethod.GET)
    private Map<String, Object> getStudentById(Integer id) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        Student student = studentService.queryStudentById(id);
        modelMap.put("student", student);
        return modelMap;
    }

    @RequestMapping(value = "addStudent", method = RequestMethod.POST)
    private Map<String, Object> addStudent(@RequestBody Student student) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put("success", studentService.addStudent(student));
        return modelMap;
    }

    @RequestMapping(value = "modifyStudent", method = RequestMethod.POST)
    private Map<String, Object> modifyStudent(@RequestBody Student student) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put("success", studentService.modifyStudent(student));
        return modelMap;
    }

    @RequestMapping(value = "deleteStudent", method = RequestMethod.GET)
    private Map<String, Object> deleteStudent(@RequestBody Integer id) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put("success", studentService.deleteStudent(id));
        return modelMap;
    }
}