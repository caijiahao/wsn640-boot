package com.scau.demo.dao;

import com.scau.demo.entity.Student;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;

import java.util.List;

public interface StudentDao {
    /**
     * 列出学生列表
     *
     * @return StudentList
     */
    List<Student> queryStudent();

    /**
     * 根据ID列出具体学生
     * @return Student
     */
    Student queryStudentById(int id);

    /**
     * 插入学生信息
     * @param student
     * @return
     */
    int insertStudent(Student student);

    /**
     * 更新学生信息
     * @param student
     * @return
     */
    int updateStudent(Student student);

    /**
     * 删除学生信息
     * @param id
     * @return
     */
    int deleteStudent(int id);

}
