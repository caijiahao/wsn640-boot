package com.scau.demo.service;

import com.scau.demo.entity.Student;

import java.util.List;

public interface StudentService {
    /**
     * 列出学生列表
     *
     * @return StudentList
     */
    List<Student> getStudentList();

    /**
     * 根据ID列出具体学生
     * @return Student
     */
    Student queryStudentById(int id);

    /**
     * 插入学生信息
     * @param student
     * @return
     */
    boolean addStudent(Student student);

    /**
     * 更新学生信息
     * @param student
     * @return
     */
    boolean modifyStudent(Student student);

    /**
     * 删除学生信息
     * @param id
     * @return
     */
    boolean deleteStudent(int id);
}
