package com.scau.demo.dao;

import com.scau.demo.entity.Student;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.*;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentDaoTest {

    @Autowired
    private  StudentDao studentDao;

    @Test
    public void queryStudent() {
        List<Student> studentList = studentDao.queryStudent();
        assertEquals(  4, studentList.size());
        for (Student student: studentList) {
            System.out.println(student.getId() + " " + student.getCls());
        }
    }

    @Test
    public void queryStudentById() {
        Student student = studentDao.queryStudentById(1);
        assertEquals("grade1", student.getGrade());


    }

    @Test
    @Ignore
    public void insertStudent() {
        Student student = new Student();
        student.setName("Peter");
        student.setAge(17);
        student.setDateEnrollment(new Date());
        student.setCls("Class6");
        int effectedNum = studentDao.insertStudent(student);
        assertEquals(1, effectedNum);
    }

    @Test
    @Ignore
    public void updateStudent() {
        Student student = studentDao.queryStudentById(4);
        student.setName("Su");
        student.setCls("Class3");
        int effectedNum = studentDao.updateStudent(student);
        assertEquals(1, effectedNum);
    }

    @Test
    @Ignore
    public void deleteStudent() {
        int effectedNum = studentDao.deleteStudent(3);
        assertEquals(1, effectedNum);
    }
}